#include <stdio.h>
int main()
{
  int r,c,i,j;
  printf("Enter the number of rows: ");
  scanf("%d",&r);
  printf("Enter the number of columns: ");
  scanf("%d",&c);
  int arr[r][c],arr_T[c][r];
  for(i=0;i<=r-1;i++){
    for(j=0;j<=c-1;j++){
      printf("Enter the value of arr[%d][%d]: ",i,j);
      scanf("%d",&arr[i][j]);
    }
  }
  for(i=0;i<=r-1;i++){
    for(j=0;j<=c-1;j++){
      arr_T[j][i] = arr[i][j];
    }
  }
  printf("ORIGINAL: \n");
  for(i=0;i<=r-1;i++){
    for(j=0;j<=c-1;j++){
      printf("%d\t",arr[i][j]);
    }printf("\n");
  }
  printf("TRANSPOSE: \n");
  for(i=0;i<=c-1;i++){
    for(j=0;j<=r-1;j++){
      printf("%d\t",arr_T[i][j]);
    }printf("\n");
  }
  return 0;
}