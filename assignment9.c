#include <stdio.h>
int main()
{
  
  int n,i;
  printf("Enter the number of elements in the array: \n");
  scanf("%d",&n);
  int arr[n];
  printf("Enter the elements of the array (sorted): \n");
  for(i=0;i<=n-1;i++){
    scanf("%d",&arr[i]);
  }
  int beg=0,mid,end=n-1,num;
  printf("Enter the number to be searched: \n");
  scanf("%d",&num);
  while(beg<=end){
    mid = (beg+end)/2;
    if(num==arr[mid]){
      printf("The number is present.");
      break;
    }
    else if(num>arr[mid]){
      beg = mid+1;
    }
    else if(num<arr[mid]){
      end = mid-1;
    }
  }
  if(beg>end){
    printf("The number is not present");
    return 0;
  }
  else{
    return 0;
  }
}