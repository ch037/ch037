#include <stdio.h>
#include <math.h>
int main()
{
    int a,b,c;
    float disc,root1,root2;
    printf("Enter the co-efficients of the quadratic equation: \n");
    scanf("%d %d %d",&a,&b,&c);
    disc = ((b*b)-(4*a*c));
    if (disc > 0)
    {
        root1 = ((-b + sqrt(disc)) / (2*a));
        root2 = ((-b - sqrt(disc)) / (2*a));
        printf("The roots of the quadratic equation are real and distinct and given by %f and %f",root1,root2);
    }
    else if (disc==0)
    {
        root1 = ((-b + sqrt(disc)) / (2*a));
        printf("The roots of the equation are real and equal and is given by %f",root1);
    }
    else
    {
        printf("The roots of the equation are imaginary");
    }
    return 0;
}


