#include <stdio.h>
int main()
{
  int num;
  printf("Enter a number: \n");
  scanf("%d",&num);
  int orig = num;
  int rev=0;
  int rem;
  while(num!=0)
  {
    rem = num % 10;
    num = num / 10;
    rev = (10*rev) + rem;
  }
  printf("The reversed number is %d",rev);
  return 0;
}