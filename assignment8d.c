#include <stdio.h>
int main()
{
  int num;
  printf("Enter the number of students: \n");
  scanf("%d",&num);
  float marks[num-1];
  int i;
  for(i=0;i<=num-1;i++)
  {
    printf("Enter the marks of student %d: ",i);
    scanf("%f",&marks[i]);
    printf("\n");
  }
  float sum = 0.0;
  for(i=0;i<=num-1;i++)
  {
    sum = sum + marks[i];
  }
  float avg;
  avg = sum / num;
  printf("Average of %d students whose total marks is %f is %f",num,sum,avg);
  return 0;
}