#include <stdio.h>
int main()
{
  int stu=5,sub=3,lar,highest[sub];
  int marks[stu][sub];
  int i,j;
  for(i=0;i<=stu-1;i++){
    for(j=0;j<=sub-1;j++){
      printf("Enter the marks of student %d in subject %d: ",i,j);
      scanf("%d",&marks[i][j]);
    }
  }
  for(j=0;j<=sub-1;j++){
    lar=marks[0][j];
    for(i=0;i<=stu-1;i++){
      if(marks[i][j]>=lar){
        lar = marks[i][j];
      }
      else{
        lar = lar;
      }
    }
    highest[j] = lar;
  }
  printf("MARKS: \n");
  for(i=0;i<=stu-1;i++){
    for(j=0;j<=sub-1;j++){
      printf("%d\t",marks[i][j]);
    }printf("\n");
  }
  printf("HIGHEST MARKS: \n");
  for(j=0;j<=sub-1;j++){
    printf("The highest marks in subject %d is %d.\n",j,highest[j]);
  }
  return 0;
}