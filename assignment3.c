#include <stdio.h>
#include <math.h>
int main()
{
	float x1, y1, x2, y2, dist;
	printf("Enter the coordinates of the first point:\n");
	scanf("%f,%f",&x1,&y1);
	printf("Enter the coordinates of the second point:\n");
	scanf("%f,%f",&x2,&y2);
	dist = sqrt(pow(x2-x1,2)+pow(y2-y1,2));
	printf("Distance between the two points (%f,%f) and (%f,%f) is %f\n", x1,y1,x2,y2,dist);
	return 0;
}


