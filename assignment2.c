#include <stdio.h>
int main()
{
	float radius, area;
	float pi = 3.141;
	printf("Enter the radius of the circle:\n");
	scanf("%f", &radius);
	area = pi*radius*radius;
	printf("Area of the circle of radius %f is: %f", radius, area);
	return 0;
}


