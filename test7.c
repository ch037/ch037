#include <stdio.h>
int main()
{
  struct student{
    char name[20];
    int roll_no;
    char course[25];
    int marks;
  }stud1,stud2;
  printf("Enter the name of student 1: ");
  scanf("%s",stud1.name);
  printf("Enter the roll no of student 1: ");
  scanf("%d",&stud1.roll_no);
  printf("Enter the course of student 1: ");
  scanf("%s",stud1.course);
  printf("Enter the marks of student 1: ");
  scanf("%d",&stud1.marks);
  printf("Enter the name of student 2: ");
  scanf("%s",stud2.name);
  printf("Enter the roll no of student 2: ");
  scanf("%d",&stud2.roll_no);
  printf("Enter the course of student 2: ");
  scanf("%s",stud2.course);
  printf("Enter the marks of student 2: ");
  scanf("%d",&stud2.marks);
  if(stud1.marks>stud2.marks){
    printf("%s has scored higher marks than %s.\n",stud1.name,stud2.name);
  }
  else if(stud2.marks>stud1.marks){
    printf("%s has scored higher marks than %s.\n",stud2.name,stud1.name);
  }
  else{
    printf("Both %s and %s have scored the same marks.\n",stud1.name,stud2.name);
  }
  return 0;
}