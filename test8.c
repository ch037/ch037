#include <stdio.h>
int main()
{
  int num,i;
  printf("Enter the number of employees: \n");
  scanf("%d",&num);
  struct join_date{
    int d;
    int m;
    int y;
  };
  struct details{
    int emp_id;
    char emp_name[50];
    int emp_salary;
    struct join_date date;
  }employee[num];
  for(i=0;i<=num-1;i++){
    printf("Enter the Employee %d ID: \n",i);
    scanf("%d",&employee[i].emp_id);
    printf("Enter the name of the employee %d: \n",i);
    scanf("%s",employee[i].emp_name);
    printf("Enter the salary of the employee %d: \n",i);
    scanf("%d",&employee[i].emp_salary);
    printf("Enter the joining date of the employee %d(D M Y): \n",i);
    scanf("%d %d %d",&employee[i].date.d,&employee[i].date.m,&employee[i].date.y);
  }
  printf("\n\n");
  printf("***** DETAILS *****");
  printf("\n\n");
  for(i=0;i<=num-1;i++){
    printf("** EMPLOYEE %d **\n",i);
    printf("EMPLOYEE ID: %d\n",employee[i].emp_id);
    printf("NAME: %s\n",employee[i].emp_name);
    printf("SALARY: %d\n",employee[i].emp_salary);
    printf("JOINING DATE: %d-%d-%d\n",employee[i].date.d,employee[i].date.m,employee[i].date.y);
    printf("\n\n");
  }
  return 0;
}