#include <stdio.h>
int main()
{
  int n = 5;
  int arr[6] = {2,4,5,6,8,10};
  int i;
  int pos;
  for (i=0;i<=n;i++)
  {
    if (arr[i]%2!=0){
      pos = i;
    }
  }
  printf("Original: ");
  printf("[");
  int j;
  for(j=0;j<=n;j++)
  {
    printf("%d,",arr[j]);
  }printf("\b]");
  printf("\n");
  for (i=pos;i<=n;i++)
  {
    arr[i] = arr[i+1];
  }
  printf("Modified: ");
  printf("[");
  for(j=0;j<=n-1;j++)
  {
    printf("%d,",arr[j]);
  }printf("\b]");
  printf("\n");
  return 0;
}