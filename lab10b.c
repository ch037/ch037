#include <stdio.h>
int main()
{
  char str[20];
  int i;
  printf("Enter the string: ");
  gets(str);
  printf("ORIGINAL: \n");
  puts(str);
  while(str[i]!='\0'){
    if((str[i]>='a')&&(str[i]<='z')){
      str[i] = str[i];
    }
    else{
      str[i] = str[i] + 32;
    }
    i++;
  }
  printf("MODIFIED: \n");
  puts(str);
  return 0;
}