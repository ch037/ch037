#include <stdio.h>
float area(int r)
{
    float pi = 3.141;
    float area_circ;
    area_circ = pi*(r*r);
    return area_circ;
}
int main()
{
    float area_circle;
    int radius;
    printf("Enter the radius: \n");
    scanf("%d",&radius);
    area_circle = area(radius);
    printf("The area of the circle with radius %d is %f",radius,area_circle);
    return 0;
}


