#include <stdio.h>
int main()
{
  int i=1;
  int num;
  int factors=0;
  printf("Enter a number: \n");
  scanf("%d",&num);
  while((i<=num)&&(num!=1))
  {
    if (num%i==0){
      factors++;
      i++;
    }
    else{
      i++;
    }
  }
  if (num==1){
    printf("The entered number %d is neither prime nor composite",num);
  }
  else if (factors==2){
    printf("The entered number %d is a prime number.",num);
  }
  else{
    printf("The entered number %d is a composite number.",num);
  }
  return 0;
}