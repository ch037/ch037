#include <stdio.h> 
void swap(int *ptr1, int *ptr2){
  int temp;
  temp = *ptr1;
  *ptr1 = *ptr2;
  *ptr2 = temp;
}
int main()
{
  int num1, num2;
  printf("Enter two numbers to swap: \n");
  scanf("%d %d",&num1,&num2);
  printf("The numbers before swapping are num1 = %d and num2 = %d\n",num1,num2);
  swap(&num1,&num2);
  printf("The numbers after swapping are num1 = %d and num2 = %d\n",num1,num2);
  return 0;
}