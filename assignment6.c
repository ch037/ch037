#include <stdio.h>
int main()
{
  int orig;
  printf("Enter the number: \n");
  scanf("%d",&orig);
  int num = orig;
  int rem;
  int rev = 0;
  do
  {
    rem = orig%10;
    orig = orig/10;
    rev = (rev*10) + rem;
  }while(orig!=0);
  if (num==rev){
    printf("The given number is a Palindrome.\n");
  }
  else{
    printf("The number is not a Palindrome.\n");
  }
  return 0;
}