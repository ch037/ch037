#include <stdio.h>
int main()
{
  int n = 5;
  int arr[6] = {2,3,5,11,13};
  int i;
  int pos = 3;
  printf("Original: ");
  printf("[");
  int j;
  for(j=0;j<=n-1;j++)
  {
    printf("%d,",arr[j]);
  }printf("\b]");
  printf("\n");
  for (i=n;i>=pos;i--)
  {
    arr[i] = arr[i-1];
  }
  arr[pos] = 7;
  printf("Modified: ");
  printf("[");
  for(j=0;j<=n;j++)
  {
    printf("%d,",arr[j]);
  }printf("\b]");
  printf("\n");
  return 0;
}