#include <stdio.h>
int main()
{
  char str[20];
  printf("Enter the string: \n");
  gets(str);
  int len = 0;
  int i=0,j,k;
  while(str[i]!='\0'){
    len++;
    i++;
  }
  printf("The length of the string is %d.\n",len);
  int mid;
  if(len%2==0){
    mid = (len/2)-1;
  }
  else{
    mid = len/2;
  }
  int exec=0;
  for(j=0,k=len-1;((j<=mid)&&(k>=mid));j++,k--){
    if(exec<=mid+1){
      if(str[j]==str[k]){
        exec++;
        continue;
      }
      else{
        break;
      }
    }
    else{
      break;
    }
    }

  if(exec==mid+1){
    printf("The given string is a palindrome.");
  }
  else{
    printf("The given string is not a palindrome.");
  }
  return 0;
}