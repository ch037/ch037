#include <stdio.h>
int main()
{
    char chara;
    printf("Enter a character:\n");
    scanf("%c", &chara);
    if (chara == 'a' || chara == 'e' || chara == 'i' || chara == 'o' || chara == 'u' || chara == 'A' || chara == 'E' || chara == 'I' || chara == 'O' || chara == 'U') {
        printf("It is a vowel");
    }
    else {
        printf("It is not a vowel");
    }
    return 0;
}

